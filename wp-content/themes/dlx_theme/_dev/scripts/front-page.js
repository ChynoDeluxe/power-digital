$( document ).ready( function () {

  var SmoothScroll = ( function () {
    $( 'a[href*="#"]' ).on( 'click', function ( e ) {
      if ( window.innerWidth <= 900 && this.parentElement.classList.contains( 'menu-item' ) ) {
        document.body.classList.remove( 'noScroll' );
        document.querySelector( '.main-navigation' ).classList.remove( 'show-menu' );
        return;
      }
      e.preventDefault();

      var target = this.hash;
      var $target = $( target );

      $( 'html, body' ).stop().animate( {
        'scrollTop': $target.offset().top - $( 'nav' ).outerHeight()
      }, 500, 'swing', function () {
        window.location.hash = target;
      } );

    } );
  }() );

  var PrimaryMenu = ( function () {
    var mainMenu = document.querySelector( '.main-navigation' );
    var mobileMenuToggle = document.getElementById( 'mobile-menu-toggle' );
    var mobileMenuClose = document.getElementById( 'mobile-menu-close' );

    function toggleMobileMenu() {
      if ( mainMenu.classList.contains( 'show-menu' ) ) {
        document.body.classList.remove( 'noScroll' );
        mainMenu.classList.remove( 'show-menu' );
      } else {
        document.body.classList.add( 'noScroll' );
        mainMenu.classList.add( 'show-menu' );
      }
    }

    mobileMenuToggle.addEventListener( 'click', toggleMobileMenu );
    mobileMenuClose.addEventListener( 'click', toggleMobileMenu );

    window.addEventListener( 'resize', function ( e ) {
      if ( window.innerWidth > 900 ) {
        if ( mainMenu.classList.contains( 'show-menu' ) ) {
          toggleMobileMenu();
        }
      }
    } );
  }() );

  var CopyYear = ( function () {
    document.querySelector( '.site-footer__copy-year' ).innerText = new Date().getFullYear();
  }() );

  var peoplePop = ( function () {
    var _body = document.body;
    var triggers = document.querySelectorAll( ".people-item__btn" );
    var close = document.querySelector( '.pop__close' );
    var popup = document.querySelector( ".pop" );

    function updatePopup( data ) {
      var person_img = document.querySelector( '.person-pop__img img' );
      var person_headline = document.querySelector( '.person-pop__headline' );
      var person_desc = document.querySelector( '.person-pop__desc' );

      person_img.src = data.personImg;
      person_headline.innerText = data.personName;
      person_desc.innerHTML = data.personDesc;
    }

    triggers.forEach( function ( btn ) {
      btn.addEventListener( "click", function ( e ) {
        e.preventDefault();
        _body.classList.add( "noScroll", "blurred" );
        popup.style.display = "flex";
        updatePopup( this.dataset );
      } );
    } );

    popup.addEventListener( "click", function ( e ) {
      var el = e.target;
      if ( el.className === "pop" ) {
        _body.classList.remove( "noScroll", "blurred" );
        popup.style.display = "none";
      } else {
        return;
      }
    } );

    close.addEventListener( 'click', function () {
      _body.classList.remove( "noScroll", "blurred" );
      popup.style.display = "none";
    } );

  }() );

  var slickSliders = ( function () {

    $( '.people .people__list' ).slick( {
      showArrows: true,
      prevArrow: '#people-slider-prev',
      nextArrow: '#people-slider-next',
      variableWidth: true
    } );
    $( '.gallery .slider__list' ).slick( {
      centerMode: true,
      centerPadding: "15vw",
      prevArrow: '#gallery-slider-prev',
      nextArrow: '#gallery-slider-next',
    } );
  }() );

  AOS.init();
} );