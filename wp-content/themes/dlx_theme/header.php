<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package dlx
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="https://gmpg.org/xfn/11">
  <?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
  <section class="section primary-header">
    <div class="container">
      <div class="share-widget">
        <div class="share-widget__text">Share</div>
        <div class="share-widget__nav">
          <a href="#" class="share-widget__nav-item"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/icons/icon-facebook.svg' ?>" alt=""></a>
          <a href="#" class="share-widget__nav-item"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/icons/icon-twitter.svg' ?>" alt=""></a>
          <a href="#" class="share-widget__nav-item"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/icons/icon-pinterest.svg' ?>" alt=""></a>
        </div>
      </div>

      <div id="mobile-menu-toggle" class="mobile-menu-toggle"><i class="fas fa-lg fa-fw fa-bars"></i></div>
      <nav id="site-navigation" class="main-navigation" data-aos="fade-left">
        <div id="mobile-menu-close" class="mobile-menu-close"><i class="fas fa-lg fa-fw fa-times"></i></div>
        <?php wp_nav_menu( array(
			'theme_location' => 'primary',
			'container' => false,
			'menu_id' => 'nav-primary',
			'menu_class' => 'primary-header__nav',
			'depth' => 0
			) ); ?>
      </nav>
    </div>
  </section>

  <div class="content">