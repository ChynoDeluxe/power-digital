<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package dlx
 */

?>

</div><!-- #content -->
<footer class="site-footer">
  <div class="container">
    <div class="site-footer__logo"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/powerdigitalmarketing-logo.png' ?>" alt=""></div>
    <div class="site-footer__copyright">&copy; <span class="site-footer__copy-year"></span> Power Digital Marketing. All rights reserved.</div>
  </div>
</footer>

<?php wp_footer(); ?>
</body>

</html>