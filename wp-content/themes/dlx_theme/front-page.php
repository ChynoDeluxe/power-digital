<?php 
  get_header();
?>

<section class="section hero-section" style="background-image: url(<?php echo get_stylesheet_directory_uri() . '/assets/img/mountainscape.jpg' ?>)">
  <h1 class="hero-section__headline section-title">Adventure</h1>
  <div data-aos="fade-right" class="hero-section__overlay-img" style="background-image: url(<?php echo get_stylesheet_directory_uri() . '/assets/img/dude.png' ?>)"></div>
</section>
<?php 
  $peopleArgs = array('post_type' => 'People', 'posts_per_page' => '4');
  $People = new WP_Query($peopleArgs);

  if($People -> have_posts()) : 
?>
<section class="section people">
  <h2 id="people" class="section-title section-title--right" data-aos="fade-left">People</h2>
  <div class="container">
    <div class="slider">
      <div class="slider__controller">
        <span class="slider__controller-text">Travelers</span>
        <div class="slider__controller-actions">
          <button id="people-slider-prev" class="slider__controller-prev"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/icons/icon-left.svg' ?>" alt=""></button>
          <button id="people-slider-next" class="slider__controller-next"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/icons/icon-right.svg' ?>" alt=""></button>
        </div>
      </div>
      <div class="people__list">
        <?php 
          while ($People->have_posts()) : $People-> the_post()
        ?>
        <div class="people-item" data-aos="fade-right">
          <div class="people-item__img"><?php echo the_post_thumbnail('full') ?></div>
          <div class="people-item__info">
            <div class="people-item__headline"><?php echo the_title() ?></div>
            <button class="people-item__btn" data-person-img="<?php echo the_post_thumbnail_url('full') ?>" data-person-name="<?php echo the_title() ?>" data-person-desc="<?php  echo strip_tags(get_the_content()) ?>">View Bio</button>
          </div>
        </div>
        <?php 
          endwhile; ?>
      </div>
    </div>
  </div>
</section>
<?php endif;?>
<section class="section places">
  <h2 id="places" class="section-title" data-aos="fade-right">Places</h2>
  <div class="container">
    <div class="places-item">
      <div class="places-item__img " data-aos="fade-left"><img class="align-right" src="<?php echo get_stylesheet_directory_uri() . '/assets/img/antelope-canyon.jpg' ?>" alt=""></div>
      <div class="places-item__content" data-aos="fade-up">
        <h3 class="places-item__headline">Antelope Canyon</h3>
        <div class="places-item__text">
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates reprehenderit atque nulla sunt quod optio excepturi dolor, ex exercitationem repellat eaque debitis error, tenetur ipsum molestiae maiores, a ducimus alias.</p>
        </div>
      </div>
    </div>
    <div class="places-item places-item--alt">
      <div class="places-item__img" data-aos="fade-right"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/lake-pehoe-torres-del-paine.jpg' ?>" alt=""></div>
      <div class="places-item__content align-right" data-aos="fade-up">
        <h3 class="places-item__headline">Torres Del Paine</h3>
        <div class="places-item__text">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lacus velit, venenatis sed orci a, sollicitudin sagittis orci. Curabitur a nisi at velit consequat tempor vel in leo. Aliquam erat volutpat. Donec semper ante massa, et mollis est sollicitudin quis. Pellentesque faucibus lacus vitae tellus porta facilisis. Vestibulum porttitor accumsan eros vitae eleifend.</p>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section gallery text-center">
  <h2 id="gallery" class="section-title" data-aos="fade-up">Gallery</h2>
  <div class="slider">
    <div class="slider__controller" data-aos="fade-up">
      <div class="slider__controller-actions  align-center">
        <button id="gallery-slider-prev" class="slider__controller-prev"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/icons/icon-left.svg' ?>" alt=""></button>
        <button id="gallery-slider-next" class="slider__controller-next"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/icons/icon-right.svg' ?>" alt=""></button>
      </div>
    </div>
    <div class="slider__list" data-aos="fade-left">
      <img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/moscow-cathedral.jpg' ?>" alt="">
      <img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/santorini-travel.jpg' ?>" alt="">
      <img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/mt-fuji.jpg' ?>" alt="">
    </div>
  </div>
</section>

<section class="pop">
  <div class="pop__body">
    <div class="person-pop">
      <div class="person-pop__img">
        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/anthony-bourdain.jpg' ?>" alt="">
      </div>
      <div class="person-pop__content">
        <h3 class="person-pop__headline">Anthony Bourdain</h3>
        <p class="person-pop__desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quaerat, dolorum laudantium accusamus quibusdam magni non harum numquam! Sunt ducimus, reprehenderit nam excepturi deleniti autem, magnam ab inventore similique dolores ipsam quod nulla explicabo vitae ipsum, culpa voluptatibus. Reiciendis, id?</p>
        <button class="btn pop__close">Close Bio</button>
      </div>
    </div>
  </div>
</section>
<?php get_footer() ?>