const gulp = require( 'gulp' );
const sass = require( 'gulp-sass' );
const rename = require( 'gulp-rename' );
const autoprefix = require( 'gulp-autoprefixer' );
const uglify = require( 'gulp-uglify-es' ).default;
const sourcemaps = require( 'gulp-sourcemaps' );
const browserSync = require( 'browser-sync' ).create();
const imagemin = require( 'gulp-imagemin' );
const del = require( 'del' );

const { paths } = require( './config.json' );

const compileSass = done => {
  return gulp.src( paths.styles.src )
    .pipe( sourcemaps.init() )
    .pipe( sass( { outputStyle: "compressed" } ).on( 'error', sass.logError ) )
    .pipe( autoprefix( { browsers: [ 'last 2 versions' ], cascade: false } ) )
    .pipe( rename( { suffix: '.min' } ) )
    .pipe( sourcemaps.write( "../sourcemaps/css/" ) )
    .pipe( gulp.dest( paths.styles.dest ) )
    .pipe( browserSync.stream() );
};

const server = done => {
  browserSync.init( {
    proxy: "http://powerdigital.chynodeluxe.com/",
    open: false
  } );

  done();
};

const compileJS = done => {
  return gulp.src( paths.scripts.src )
    .pipe( sourcemaps.init() )
    .pipe( uglify() )
    .pipe( rename( { suffix: '.min' } ) )
    .pipe( sourcemaps.write( '../sourcemaps/js' ) )
    .pipe( gulp.dest( paths.scripts.dest ) );
};

const watchFiles = done => {
  gulp.watch( paths.styles.src, compileSass );
  gulp.watch( paths.scripts.src, compileJS ).on( 'change', browserSync.reload );
  gulp.watch( paths.root + '**/*.php' ).on( 'change', browserSync.reload );
  gulp.watch( paths.images.src, copyImages );
};

const copyImages = done => {
  return gulp.src( paths.images.src )
    .pipe( imagemin( {
      interlaced: true,
      progressive: true,
      optimizationLevel: 5
    } ) )
    .pipe( gulp.dest( paths.images.dest ) );
};

const clean = done => {
  del( paths.root + 'assets/**', { force: true } );
  done();
};

exports.default = gulp.series( clean, copyImages, compileSass, compileJS, gulp.parallel( server, watchFiles ) );