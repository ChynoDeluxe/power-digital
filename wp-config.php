<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         's9FSGd7Bb4yK9wXJVGeYCSki5u7XpsLy0m5vAHf9GkT5Ukuu9n4CbHZtpwxGgsDlwwUEu0DU1QWG+HRhNg3tbA==');
define('SECURE_AUTH_KEY',  'xAYt9orq9Xb3Vb23zawNSR52rYnp4ITKnCWkvb+5Av1Xdv9Zsu8hi6jMTzOlGzRgzc/XTiBZUWzewkNzI1SkVQ==');
define('LOGGED_IN_KEY',    'Nao5U1G7wKauy9vbty/Nii5AEAAq9eLzKaSyNzfYhsz1CdvJOE9VaWpkBecoGpmTI3YLt/hu15UsEMAjTKNF3g==');
define('NONCE_KEY',        'As8MHVwzKdH3p26G60xJ+0AmGtP2+MuGldHXjfnkCDLvJ087Ex05iYfE5vlRUZ3swSMmim6qsvdhNS6k8n6Img==');
define('AUTH_SALT',        'IwChAmqGpAaj4Z6F6XF+e3r/JehRNcIDIovEHECuX2quBnR5QG8689NXQnCMzPJiHDDcxgw+mswVFEH0E4GOcA==');
define('SECURE_AUTH_SALT', 'yw2uPuaqTAWSeS7pvpLLSERBqZpTC8TQLDYPJcilVclIYBfNDwJHBv+yEitpf0WTioutX76yV9eyWf6wlBBkpg==');
define('LOGGED_IN_SALT',   'hhrsVpzRVj3JrJF/gbaT+mNNmTV/ki2EgMT0kCqrXCPzN7oykS920RVhGihJL7WlvINf5m3izIuMV1zi3Wca1w==');
define('NONCE_SALT',       'JEldl0flOYswT/hIl1w4IEOnfmFH3Hh/GAmHqqVv1gHN9+gqdWnw6euruG2qPFN/pr+P0Dr3bl4E0uIEb2mw1Q==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
